class Post < ActiveRecord::Base
  # dependent: :destroy elimina los comentarios cuando se borra un post
  has_many :comments, dependent: :destroy
  # validacion de datos
  validates_presence_of :title
  validates_presence_of :body
end
